import low from "lowdb";
import lodashId from "lodash-id";
import FileSync from "lowdb/adapters/FileSync.js";

export default async function localDB() {
  //Set up lowdb
  const adapter = new FileSync("db.json");
  const db = low(adapter);
  db._.mixin(lodashId);

  //Define defaults
  db.defaults({ movies: [], characters: [], planets: [], species: [] }).write();

  //Connector implementation
  return {
    _types: { MOVIES: "movies", CHARACTERS: "characters", PLANETS: "planets", SPECIES: "species" },
    getAll(type) {
      return db.get(type).value();
    },
    get(type, id) {
      return db
        .get(type)
        .getById(id)
        .value();
    },
    find(type, match) {
      return db
        .get(type)
        .filter(match)
        .value();
    },
    add(type, data) {
      return db
        .get(type)
        .insert(data)
        .write();
    },
    remove(type, id) {
      return db
        .get(type)
        .removeById(id)
        .write();
    },
    update(type, id, data) {
      return db
        .get(type)
        .getById(id)
        .assign(data)
        .write();
    }
  };
}