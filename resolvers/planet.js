export default async function() {

    // Query implementations
    function getPlanets(root, params, { connectors: { localDB } }) {
        return localDB.getAll(localDB._types.PLANETS);
    }
    function getPlanet(root, { id }, { connectors: { localDB } }) {
        return localDB.get(localDB._types.PLANETS, id);
    }

    // Mutation implementations



    // Field implementations
    function planetGetCharacters({ id }, params, { connectors: { localDB } }) {
        return localDB.find(localDB._types.CHARACTERS, character => 
            character.homeworld == id
        );
    }

    // Schema linking
    this.Query.planets = getPlanets;
    this.Query.planet = getPlanet;
    
    this.Planet = {};
    this.Planet.characters = planetGetCharacters;
}