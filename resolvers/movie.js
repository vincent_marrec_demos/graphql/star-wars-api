export default async function() {

    // Query implementations
    function getMovies(root, params, { connectors: { localDB } }) {
        return localDB.getAll(localDB._types.MOVIES);
    }
    function getMovie(root, { id }, { connectors: { localDB } }) {
        return localDB.get(localDB._types.MOVIES, id);
    }

    // Mutation implementations
    function addMovie(root, { data }, { connectors: { localDB } }) {
        return localDB.add(localDB._types.MOVIES, Object.assign(data, { characters: []}));
    }
    function removeMovie(root, { id }, { connectors: { localDB } }) {
        return localDB.remove(localDB._types.MOVIES, id);
    }
    function movieAddCharacters(root, { id, characters }, { connectors: { localDB } }) {
        let movie = localDB.get(localDB._types.MOVIES, id);
        let dbCharacters = localDB.find(localDB._types.CHARACTERS, movie => characters.includes(movie.id))
                                  .map(character => character.id);
        
        characters.forEach(character => {
            if (!dbCharacters.includes(character) || movie.characters.includes(character)) return;
            movie.characters.push(character);
        })

        return localDB.update(localDB._types.MOVIES, id, { characters: movie.characters });
    }
    function movieRemoveCharacters(root, { id, characters }, { connectors: { localDB } }) {
        let movie = localDB.get(localDB._types.MOVIES, id);

        movie.characters = movie.characters.filter(character => !characters.includes(character));

        return localDB.update(localDB._types.MOVIES, id, { characters: movie.characters });
    }

    // Field implementations
    function movieGetCharacters({ characters }, params, { connectors: { localDB } }) {
        return characters.map(character => localDB.get(localDB._types.CHARACTERS, character));
    }

    // Schema linking
    this.Query.movies = getMovies;
    this.Query.movie = getMovie;

    this.Mutation.movie_add = addMovie;
    this.Mutation.movie_remove = removeMovie;

    this.Mutation.movie_add_characters = movieAddCharacters;
    this.Mutation.movie_remove_characters = movieRemoveCharacters;

    this.Movie = {};
    this.Movie.characters = movieGetCharacters;
}