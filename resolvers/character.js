export default async function() {

    // Query implementations
    function getCharacters(root, params, { connectors: { localDB } }) {
        return localDB.getAll(localDB._types.CHARACTERS);
    }
    function getCharacter(root, { id }, { connectors: { localDB } }) {
        return localDB.get(localDB._types.CHARACTERS, id);
    }

    // Mutation implementations


    //Field implementations
    function characterMovies({ id }, params, { connectors: { localDB } }) {
        return localDB.find(localDB._types.MOVIES, movie => 
            movie.characters.includes(id)
        );
    }
    function characterSpecies({ species }, params, { connectors: { localDB } }) {
        return species ? localDB.get(localDB._types.SPECIES, species) : null;
    }
    function characterHomeworld({ homeworld }, params, { connectors: { localDB } }) {
        return localDB.get(localDB._types.PLANETS, homeworld);
    }

    // Schema linking
    this.Query.characters = getCharacters;
    this.Query.character = getCharacter;

    this.Character = {};
    this.Character.movies = characterMovies;
    this.Character.species = characterSpecies;
    this.Character.homeworld = characterHomeworld;
}