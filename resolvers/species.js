export default async function() {

    // Query implementations
    function getSpecies(root, params, { connectors: { localDB } }) {
        return localDB.getAll(localDB._types.SPECIES);
    }
    function getSpecie(root, { id }, { connectors: { localDB } }) {
        return localDB.get(localDB._types.SPECIES, id);
    }

    // Mutation implementations



    // Field implementations
    function speciesGetCharacters({ id }, params, { connectors: { localDB } }) {
        return localDB.find(localDB._types.CHARACTERS, character => 
            character.species == id
        );
    }

    // Schema linking
    this.Query.species = getSpecies;
    this.Query.specie = getSpecie;
    
    this.Species = {};
    this.Species.characters = speciesGetCharacters;
}