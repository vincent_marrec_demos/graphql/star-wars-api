# Star Wars API

GraphQL API with basic queries and mutations

## How to use

* `npm install`
* `npm run start`
* Test the API with the Postman collection in the repository

> Be careful with the variables used in the requests when testing with postman, as you may need to change them for some requests (especially with "remove" requests)